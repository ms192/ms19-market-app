package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateMarketDto;
import az.ingress.marketapp.dto.MarketDto;

import java.util.List;

public interface MarketService {
    void create(CreateMarketDto marketDto);

    List<MarketDto> findAll();

    MarketDto findById(Long id);

    void update(Long id, CreateMarketDto marketDto);

    void delete(Long id);
}
