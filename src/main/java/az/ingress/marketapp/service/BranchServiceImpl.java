package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateBranchDto;
import az.ingress.marketapp.mapper.BranchMapper;
import az.ingress.marketapp.model.Branch;
import az.ingress.marketapp.model.Market;
import az.ingress.marketapp.repository.BranchRepository;
import az.ingress.marketapp.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final BranchMapper branchMapper;

    @Override
    public void create(Long marketId, CreateBranchDto branchDto) {
        Market market = marketRepository.findById(marketId).orElseThrow(RuntimeException::new);
        Branch branch = branchMapper.dtoToBranch(branchDto);
        branch.setMarket(market);
        branchRepository.save(branch);
    }
}
