package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateManagerDto;

public interface ManagerService {
    void create(Long marketId, Long branchId, CreateManagerDto managerDto);

}
