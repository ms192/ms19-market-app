package az.ingress.marketapp.service;

import az.ingress.marketapp.dto.CreateBranchDto;

public interface BranchService {
     void create(Long marketId, CreateBranchDto branchDto);
}
