package az.ingress.marketapp.controller;

import az.ingress.marketapp.dto.CreateBranchDto;
import az.ingress.marketapp.dto.CreateMarketDto;
import az.ingress.marketapp.dto.MarketDto;
import az.ingress.marketapp.service.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market/{marketId}/branch")
@Valid
public class BranchController {

    private final BranchService branchService;

    @PostMapping
    public void create(@PathVariable Long marketId,
                       @RequestBody CreateBranchDto branchDto) {
        branchService.create(marketId, branchDto);
    }
}
