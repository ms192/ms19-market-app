package az.ingress.marketapp.repository;

import az.ingress.marketapp.model.Market;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MarketRepository extends JpaRepository<Market, Long> {
    Optional<Market> findByName(String name);
}
