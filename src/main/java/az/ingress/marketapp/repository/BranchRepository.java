package az.ingress.marketapp.repository;

import az.ingress.marketapp.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BranchRepository extends JpaRepository<Branch, Long> {
}
